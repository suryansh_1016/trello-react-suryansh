import React from 'react'
import Checkbox from '@mui/material/Checkbox';
import CloseIcon from '@mui/icons-material/Close';
import { Box, IconButton, Stack } from '@mui/material';
import { deleteCheckItem, toggleCheckBox } from '../Api/Api';

const CheckItem = ({ name, state, id, listId, items, setItems, cardId }) => {
    const handleItemDelete = async () => {
        try {
            await deleteCheckItem(listId, id);
            setItems(items.filter(ele => ele.id !== id));
        } catch (error) {
            console.log('error', error);
        }
    };

    const handleCheckbox = async (e) => {
        const State = e.target.checked ? 'complete' : 'incomplete';
        setItems(
            items.map((data) => {
                if (data.id === id) {
                    return { ...data, state: State };
                }
                return data;
            })
        );
        try {
            await toggleCheckBox(cardId, listId, id, State);
        }
        catch (error) {
            console.log("error setting checkbox", error)

        }
    }

    return (
        <>
            <Stack direction={'row'} alignItems={'center'} justifyContent={'space-between'} textAlign={'left'} sx={{ borderRadius: '10px', marginBottom: '4px' }}>
                <Box sx={{ textDecoration: state === 'complete' ? 'line-through' : 'none' }}>
                    < Checkbox checked={state === 'complete'} sx={{ w: '25px', h: '25px' }} onChange={handleCheckbox} />
                    {name}
                </Box>
                <IconButton onClick={handleItemDelete}>
                    <CloseIcon />
                </IconButton>
            </Stack>
        </>
    )
}

export default CheckItem