import React, { useState, useEffect } from 'react';
import { Paper, Stack, TextField, Box, IconButton } from '@mui/material'
import DeleteIcon from '@mui/icons-material/Delete';
import CardItem from './CardItem';
import Add from './Add';
import { createCard, deleteList, getAllCards } from '../Api/Api';

const ListCard = ({ title, listId, setListData }) => {
  const [cards, setCards] = useState([]);
  const [cardInput, setCardInput] = useState("");

  useEffect(() => {
    const getCards = async () => {
      try {
        const res = await getAllCards(listId);
        setCards(res.data);
      } catch (error) {
        console.log("error while fetching cards", error);
      }
    }
    getCards();
  }, [])

  const handleDeleteList = async () => {
    try {
      await deleteList(listId) ;
      setListData((prev) => prev.filter((ele) => ele.id !== listId));
    } catch (error) {
      console.log("error while deleting list", error);
    }
  }

  const handleAddCard = async () => {
    try {
        const req = await createCard(listId , cardInput);
        setCards((prev) => [...prev, req.data]);
        setCardInput("");
    } catch (error) {
      console.log("error while sending cards", error);
    }
  }

  return (
    <>
      <Paper sx={{ minWidth: '300px', height: 'fit-content', padding: '0.6rem', bgcolor: '#f1f2f4', borderRadius: '12px' }}>
        <Stack sx={{ padding: '0.2rem', flexDirection: 'row', alignItems: 'center' }}>
          <TextField id="outlined-basic" variant="outlined" sx={{
            width: '90%', padding: '0rem', border: 'none',
            '& .MuiOutlinedInput-root': {
              '& fieldset': {
                borderWidth: 0,
              },
            },
          }} defaultValue={title}
            InputProps={{
              readOnly: true,
            }} />
          <Box sx={{ height: 'fit-content', width: '10%' }}>
            <IconButton onClick={handleDeleteList}>
              <DeleteIcon />
            </IconButton>
          </Box>
        </Stack>

        <Stack>
          {cards.map((card) => {
            return <CardItem key={card.id} name={card.name} id={card.id} setCards={setCards} cards={cards} cardId={card.id} />
          })}
        </Stack>

        <Stack>
          <Add add={"Add Card"} setText={setCardInput} handleSubmit={handleAddCard} />
        </Stack>
      </Paper>
    </>
  )
}

export default ListCard