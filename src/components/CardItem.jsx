import { useState } from 'react';
import { Button } from '@mui/material'
import AddPopup from './AddPopup';

const CardItem = ({ name, id, setCards, cards , cardId }) => {
    const [open, setOpen] = useState(false);
    const handleClickOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);

    return (
        <>
            <Button
                variant='contained'
                sx={{ width: '100%', justifyContent: 'flex-start', bgcolor: '#fff', borderRadius: "10px", color: 'black', textTransform: 'none', boxShadow: 'none', marginBottom:'0.6rem' ,
                    '&:hover': { bgcolor: 'rgba(0, 0, 0, 0.1)', boxShadow: 'none'
                    }
                }}
                onClick={handleClickOpen}
            >
                {name}
            </Button>
            <AddPopup open={open} handleClose={handleClose} id={id} setCards={setCards} cards={cards} name={name} cardId={cardId} />
        </>
    )
}

export default CardItem