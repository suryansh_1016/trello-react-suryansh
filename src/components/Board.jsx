import React from 'react'
import { Paper, Typography } from '@mui/material'
import { useNavigate } from 'react-router-dom'

const Board = ({ name, id }) => {
    const navigate = useNavigate();
    return (
        <Paper elevation={3} 
        sx={{
            background: '#fff',
            backgroundSize: 'cover',
            borderRadius: '10px',
            display: "flex", justifyContent: "center", alignItems: "center",
            transition: 'background-color 0.3s ease-in-out',
            '&:hover': {
                background: "rgba(0 , 0 , 0, 0.3)",
                cursor: 'pointer'
            }
        }}
        onClick={() => navigate(`/boards/${id}`)}
        >
            <Typography variant='h6' fontWeight='bold' >
                {name}
            </Typography>
        </Paper>
    );
};

export default Board