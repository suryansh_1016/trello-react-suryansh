import React, { useEffect, useState } from 'react'
import { Box, IconButton, Paper, Stack, Typography } from "@mui/material";
import Add from "./Add";
import CheckItem from './CheckItem';
import CancelIcon from '@mui/icons-material/Cancel';
import TaskAltIcon from '@mui/icons-material/TaskAlt';
import CircularProgress from '@mui/material/CircularProgress';
import LinearProgress from '@mui/material/LinearProgress';
import { createCheckItem, deleteChecklist, getAllCheckItems } from '../Api/Api';
import { useSnackbar } from "notistack";

const Checklist = ({ name, id, checklists, setChecklists, cardId }) => {
    const [checkItem, setCheckItem] = useState("");
    const [items, setItems] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    const { enqueueSnackbar } = useSnackbar();

    useEffect(() => {
        const getCheckItems = async () => {
            try {
                const res = await getAllCheckItems(id);
                setItems(res.data);
            } catch (error) {
                console.error('Error fetching check items:', error);
            } finally {
                setIsLoading(false)
            }
        };
        getCheckItems();
    }, []);

    const handleCheckItemSubmit = async () => {
        const req = await createCheckItem(id, checkItem);
        setItems([...items, req.data]);
        enqueueSnackbar("Board Created Successfully", {
            variant: "success",
        });
    }

    const handleChecklistDelete = async () => {
        try {
            await deleteChecklist(id)
            setChecklists(checklists.filter(ele => ele.id !== id));
        } catch (error) {
            console.log('error', error);
        }
    }

    const handleCheckboxChange = () => {
        const totalItems = items.length;
        const checkedItems = items.filter(item => item.state === 'complete').length;
        const progress = (checkedItems / totalItems) * 100;
        return Math.floor(progress);
    }

    return (
        <>
            <Paper
                elevation={3}
                sx={{
                    width: '100%', padding: '0.6rem', display: "flex", flexDirection: "column", justifyContent: 'flex-start', bgcolor: '#fff', borderRadius: "10px", color: 'black', textTransform: 'none', boxShadow: 'none', mb: '1rem',
                    '&:hover': { bgcolor: '#fff', boxShadow: 'none' }
                }}
            >
                <Stack direction={'row'} alignItems={'center'} justifyContent={'space-between'} sx={{ paddingBottom: '0.6rem' }} >
                    <Stack direction={'row'} alignItems={'center'} gap={'0.6rem'}>
                        <TaskAltIcon sx={{ color: '#1976d2' }} />
                        <Typography variant='h6'>
                            {name}
                        </Typography>
                    </Stack>


                    <Box sx={{ height: '25px', width: '25px', display: 'flex', alignItems: "center", justifyContent: "center" }}>
                        <IconButton onClick={handleChecklistDelete}>
                            <CancelIcon sx={{ '&:hover': { color: 'red' } }} />
                        </IconButton>
                    </Box>
                </Stack>

                {
                    items.length > 0 ? (<Box sx={{ width: '100%', display: 'flex', alignItems: 'center', marginBottom: '10px' }}>
                        <LinearProgress sx={{ height: '5px', borderRadius: '10px', flexGrow: 1, alignItems: 'center' }} variant="determinate" value={handleCheckboxChange()} />
                        <Typography variant='h8' sx={{ marginLeft: '0.5rem' }}>
                            {`${handleCheckboxChange()}%`}
                        </Typography>
                    </Box>) : (<></>)
                }

                <Stack>
                    {
                        isLoading ? (<Box sx={{ display: 'flex', justifyContent: 'center', width: '100%' }}>
                            <CircularProgress />
                        </Box>
                        ) : (items.map((ele) => {
                            return (
                                <CheckItem key={ele.id} name={ele.name} state={ele.state} id={ele.id} listId={id} items={items} setItems={setItems} cardId={cardId} />
                            )
                        }))
                    }
                </Stack>

                <Stack sx={{ flexDirection: "row" }}>
                    <Add add={"Add Item"} setText={setCheckItem} handleSubmit={handleCheckItemSubmit} placeholder={"Enter Item Name ..."} />
                </Stack>
            </Paper>
        </>
    )
}

export default Checklist