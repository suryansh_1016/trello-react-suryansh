import React, { useState } from 'react'
import { Button, Stack, Box, TextField, IconButton } from '@mui/material'
import AddIcon from '@mui/icons-material/Add'
import CloseIcon from '@mui/icons-material/Close';

const Add = ({ add, setText, handleSubmit, placeholder, button }) => {

    const [show, setShow] = useState(false);

    const handleClose = () => {
        setShow(false)
    }
    const handleOpen = () => {
        setShow(true)
    }

    const handleKeyDown = (e) => {
        if (e.key === 'Enter' && !e.shiftKey) {
            handleFormSubmit(e);
        }
    };

    const handleFormSubmit = (e) => {
        e.preventDefault();
        handleClose()
        handleSubmit()
    }

    return (
        <>
            {!show && (<Button
                variant='contained'
                sx={{
                    bgcolor: 'transparent', color: 'black', borderRadius: "10px", boxShadow: 'none',
                    '&:hover': { bgcolor: 'rgba(0, 0, 0, 0.1)', boxShadow: 'none' }
                }}
                onClick={handleOpen}
            >
                <AddIcon />
                {add}
            </Button>)
            }
            
            {show && (
                <form style={{width:'100%'}}
                    onSubmit={handleFormSubmit}
                >
                    <Stack
                        sx={{
                            flexDirection: 'column',
                            gap: '0.6rem',
                            alignItems: 'left',
                            width: '100%'
                        }}
                    >
                        <TextField
                            required
                            multiline
                            rows={2}
                            onKeyDown={handleKeyDown}
                            placeholder={placeholder || 'Enter a title for this card ...'}
                            onChange={(e) => { setText(e.target.value) }}
                            sx={{
                                width: '100%',
                                bgcolor: '#fff',
                                borderRadius: '8px',
                                boxShadow: '0px 2px 4px rgba(0, 0, 0, 0.1), 0px 4px 6px rgba(0, 0, 0, 0.08)',
                                '& input': {
                                    padding: '7px 8px',
                                },
                                '& .MuiOutlinedInput-root': {
                                    '& fieldset': {
                                        borderWidth: 0,
                                    },
                                },
                                '&:hover .MuiOutlinedInput-root': {
                                    '& fieldset': {
                                        borderWidth: 0,
                                    },
                                },
                            }}
                        />
                        <Stack sx={{ flexDirection: 'row' }}>
                            <Button
                                variant='contained'
                                sx={{ height: '36px', padding: '3px 6px', width: 'fit-content' }}
                                type='submit'
                            >
                                {button || add}
                            </Button>
                            <Box sx={{ height: 'fit-content' }}>
                                <IconButton sx={{ borderRadius: '5px' }} onClick={handleClose}  >
                                    <CloseIcon />
                                </IconButton>
                            </Box>
                        </Stack>
                    </Stack>
                </form>
            )}
        </>
    )
}

export default Add