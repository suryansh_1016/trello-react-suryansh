import { Dialog, DialogTitle, DialogContent, IconButton, Typography } from "@mui/material";
import DeleteIcon from '@mui/icons-material/Delete';
import Add from "./Add";
import { useEffect, useState } from "react";
import Checklist from "./Checklist";
import { createChecklist, deleteCard, getAllChecklist } from "../Api/Api";
import { useSnackbar } from "notistack";

const AddPopup = ({ open, handleClose, id, setCards, cards, name, cardId }) => {

    const [checklists, setChecklists] = useState([]);
    const [checklistTitle, setChecklistTitle] = useState("");

    const { enqueueSnackbar } = useSnackbar();

    useEffect(() => {
        const getCheckList = async () => {
            try {
                const response = await getAllChecklist(id)
                setChecklists(response.data);
                
            } catch (error) {
                console.error("Error while fetching checklists:", error);
            }
        }
        getCheckList();
    }, [])

    const deleteCardApi = async () => {
        try {
            await deleteCard(id);
            setCards(cards.filter((ele) => ele.id !== id));
            handleClose();
        } catch (error) {
            console.error('Error deleting card:', error);
        }
    }

    const handleChecklistSubmit = async () => {
        try {
            const req = await createChecklist(id, checklistTitle);
            setChecklists([...checklists, req.data]);
            setChecklistTitle("");
            enqueueSnackbar("Board Created Successfully", {
                variant: "success",
            });
        } catch (error) {
            console.log("error while add checklist");
        }
    }

    return (
        <>
            <Dialog
                open={open}
                onClose={handleClose}
            >
                <DialogTitle
                    sx={{
                        display: "flex",
                        width: "100%",
                        justifyContent: "space-between",
                        bgcolor: '#f1f2f4',
                    }}
                >
                    <Typography variant='h4'>{name}</Typography>
                    <IconButton onClick={deleteCardApi}>
                        <DeleteIcon />
                    </IconButton>
                </DialogTitle>
                <DialogContent sx={{ minWidth: '400px', bgcolor: '#f1f2f4' }}>
                    {
                        checklists.map((ele) => {
                            return <Checklist key={ele.id} name={ele.name} id={ele.id} checklists={checklists} setChecklists={setChecklists} cardId={cardId} />
                        })
                    }
                    <Add add={"Add Checklist"} setText={setChecklistTitle} handleSubmit={handleChecklistSubmit} placeholder={"Enter Checklist Name ..."} />
                </DialogContent>
            </Dialog>
        </>
    );
};

export default AddPopup;
