import axios from "axios";

const baseUrl = "https://api.trello.com/1/"

const key = import.meta.env.VITE_REACT_APP_TRELLO_API_KEY;
const token = import.meta.env.VITE_REACT_APP_TRELLO_TOKEN;

axios.defaults.params = {
    key: key,
    token: token,
}

export const getAllBoards = async () => {
    return await axios.get(`${baseUrl}members/me/boards?fields=name,url&filter=open`)
};

export const createBoard = async (boardName) => {
    return await axios.post(`${baseUrl}boards/?name=${boardName}`)
};

export const getAllLists = async (boardId) => {
    return await axios.get(`${baseUrl}boards/${boardId}/lists`)
}

export const createList = async (listName, boardId) => {
    return await axios.post(`${baseUrl}lists?name=${listName}&idBoard=${boardId}`)
}

export const deleteList = async (listId) => {
    return await axios.put(`${baseUrl}lists/${listId}/closed?value=true`)
}

export const getAllCards = async (listId) => {
    return await axios.get(`${baseUrl}lists/${listId}/cards`)
}

export const createCard = async (listId, cardName) => {
    return await axios.post(`${baseUrl}cards?idList=${listId}&name=${cardName}`)
}

export const getAllChecklist = async (cardId) => {
    return await axios.get(`${baseUrl}cards/${cardId}/checklists`)
}

export const deleteCard = async (cardId) => {
    return await axios.delete(`${baseUrl}cards/${cardId}`)
}

export const createChecklist = async (cardId, name) => {
    return await axios.post(`${baseUrl}checklists?name=${name}&idCard=${cardId}`);
}

export const deleteChecklist = async (checkListId) => {
    return await axios.delete(`${baseUrl}checklists/${checkListId}`)
}

export const getAllCheckItems = async (checkListId) => {
    return await axios.get(`${baseUrl}checklists/${checkListId}/checkItems`)
}

export const createCheckItem = async (checkListId, name) => {
    return await axios.post(`${baseUrl}checklists/${checkListId}/checkItems?name=${name}`)
}

export const deleteCheckItem = async (checkListId, checkItemId) => {
    return await axios.delete(`${baseUrl}checklists/${checkListId}/checkItems/${checkItemId}`)
}

export const toggleCheckBox = async (cardId, listId, id, State) => {
    return await axios.put(`${baseUrl}cards/${cardId}/checklist/${listId}/checkItem/${id}?key=${key}&token=${token}&state=${State}`)
}
