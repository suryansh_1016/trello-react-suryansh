import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { Box, Paper, Stack } from '@mui/material';
import ListCard from '../components/ListCard';
import Add from '../components/Add';
import CircularProgress from '@mui/material/CircularProgress';
import { createList, getAllLists } from '../Api/Api';
import { useSnackbar } from "notistack";


const ListsPage = () => {
    const { id } = useParams();
    const [listData, setListData] = useState([])
    const [listName, setListName] = useState("")
    const [isLoading, setIsLoading] = useState(true);

    const { enqueueSnackbar } = useSnackbar();

    useEffect(() => {
        const getListsApi = async () => {
            try {
                const res = await getAllLists(id);
                setListData(res.data)
            } catch (error) {
                console.log("error while fetching lists ", error)
            } finally {
                setIsLoading(false)
            }
        };
        getListsApi();
    }, [id])

    const handleListSubmit = async () => {
        try {
            const res = await createList(listName, id)
            setListData([...listData, res.data]);
            setListName("")
            enqueueSnackbar("Board Created Successfully", {
                variant: "success",
            });
        } catch (error) {
            console.log("Error occurred while creating list", error)
        }
    };

    return (
        <>
            <Box sx={{ bgcolor: '#ececec', minHeight: 'calc(100vh - 64px)', display: 'flex', flexDirection: 'column', alignItems: 'center', maxWidth: '100vw', paddingTop: '2rem' }} >
                {
                    isLoading ? (<CircularProgress />) : (
                        <Box sx={{ width: "85%", display: 'flex', gap: '2rem', overflowX: 'scroll', minHeight: '80vh' }}>
                            {listData.map((list) => {
                                return <ListCard key={list.id} title={list.name} listId={list.id} setListData={setListData} listData={listData} />
                            })
                            }
                            <Paper sx={{ minWidth: '300px', height: 'fit-content', padding: '0.5rem', bgcolor: '#fff', borderRadius: '12px' }}>
                                <Stack>
                                    <Add add={"Add List"} setText={setListName} handleSubmit={handleListSubmit} button={"Add"} placeholder={"Enter List Name ..."} />
                                </Stack>
                            </Paper>
                        </Box>
                    )
                }
            </Box>
        </>
    )
}

export default ListsPage