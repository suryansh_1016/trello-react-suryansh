import { Stack, Typography } from '@mui/material'
import React from 'react'

const ErrorPage = () => {
    return (<>
        <Stack sx={{ maxWidth: '100vw', height: "100vh", alignItems: 'center', marginTop: '2rem' }}>
            <Typography variant='h5'>
                page not found
            </Typography>
        </Stack>
    </>
    )
}

export default ErrorPage