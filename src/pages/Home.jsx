import React, { useEffect, useState } from 'react'
import { Box, Typography } from '@mui/material'
import Board from '../components/Board';
import Add from '../components/Add';
import CircularProgress from '@mui/material/CircularProgress';
import { createBoard, getAllBoards } from '../Api/Api';
import { useSnackbar } from "notistack";

const Home = () => {
    const [boards, setBoards] = useState([]);
    const [boardName, setBoardName] = useState('');
    const [isLoading, setIsLoading] = useState(true);

    const { enqueueSnackbar } = useSnackbar();

    useEffect(() => {
        const getBoards = async () => {
            try {
                const response = await getAllBoards();
                setBoards(response.data);
            } catch (error) {
                console.error("error while fetching boards", error);
            } finally {
                setIsLoading(false);
            }
        };
        getBoards();
    }, []);

    const handleCreateBoard = async () => {
        try {
            const res = await createBoard(boardName);
            setBoards(prevBoards => [...prevBoards, res.data]);
            setBoardName('');
            enqueueSnackbar("Board Created Successfully", {
                variant: "success",
            });
        } catch (error) {
            console.error('Error while creating board:', error);
        }
    };

    return (
        <>
            <Box sx={{ bgcolor: '#ececec', minHeight: 'calc(100vh - 64px)', display: 'flex', flexDirection: 'column', alignItems: 'center', maxWidth: '100vw' }} >
                <Box sx={{ width: "85%" }} >
                    <Typography sx={{ margin: '1.2rem 0', fontSize: '2.6rem' }}>
                        Your Workspace
                    </Typography>
                </Box>
                {
                    isLoading ? (<CircularProgress />) : (
                        <Box sx={{ width: "85%", display: 'flex', flexWrap: 'wrap', '& > :not(style)': { m: 1, width: 250, height: 128 } }}>
                            {boards && boards.map((board, index) => {
                                return <Board key={index} name={board.name} id={board.id} />
                            })}
                            <Add add="create a new board" placeholder={"Enter Board name"} button={"Create Board"} setText={setBoardName} handleSubmit={handleCreateBoard} />
                        </Box>
                    )
                }
            </Box>
        </>
    );
};

export default Home