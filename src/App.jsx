import './App.css'
import { Navigate, Route , Routes} from 'react-router-dom'
import Navbar from './components/Navbar'
import Home from './pages/Home'
import ListsPage from './pages/ListsPage'
import { SnackbarProvider } from 'notistack'

function App() {
  return (
    <>
    <SnackbarProvider maxSnack={3} anchorOrigin={{ vertical: 'bottom', horizontal: 'left'}}>
      <Navbar/>
      <Routes>
        <Route path='/' element={ <Navigate to ='/boards'/>} replace/>
        <Route path='/boards' element={<Home/>} />
        <Route path='/boards/:id' element={<ListsPage/>} />
      </Routes>
      </SnackbarProvider>
    </>
  )
}

export default App
